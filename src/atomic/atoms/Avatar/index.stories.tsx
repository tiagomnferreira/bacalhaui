import { ComponentStory, ComponentMeta } from "@storybook/react";
import Avatar from ".";
import { SizeVariants } from "../../../style/types";

export default {
  title: "Avatar",
  component: Avatar,
  argTypes: {
    size: {
      options: [SizeVariants.LG, SizeVariants.MD, SizeVariants.SM],
      control: { type: "select" },
    },
  },
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

export const Base = Template.bind({});
Base.args = {};

export const Label = Template.bind({});
Label.args = {
  label: "Tiago",
};

export const Image = Template.bind({});
Image.args = {
  src: "https://reactjs.org/logo-og.png",
};
