import styled from "styled-components";
import { getStyleForSize } from "./utils";
import { getBoxShadow } from "../../../style/utils";
import { Props } from "./types";
import Paragraph from "../Paragraph";

export const Container = styled.div<Omit<Props, "label">>`
  ${getStyleForSize};
  box-shadow: ${({ borderless }) =>
    !borderless && getBoxShadow({ blurRadius: 5, spreadRadius: -2 })};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${({ theme }) => theme.spacings[8]};
  box-sizing: border-box;
  overflow: hidden;
  background-image: ${({ src }) => `url(${src})`};
  background-position: center;
  background-size: cover;
`;

export const EllipsisParagraph = styled(Paragraph)`
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-align: center;
`;

export const Image = styled.img`
  max-width: 100%;
  max-height: 100%;
`;
