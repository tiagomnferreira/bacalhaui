import { StyledComponentsProps } from "../../../style/types";
import { SizeVariants } from "../../../style/types";
import { Props } from "./types";

export const getStyleForSize = ({
  size,
  theme,
}: StyledComponentsProps<Pick<Props, "size">>) => {
  let containerSize = theme.sizes[40];

  if (size === SizeVariants.SM) {
    containerSize = theme.sizes[40];
  } else if (size === SizeVariants.MD) {
    containerSize = theme.sizes[64];
  } else if (size === SizeVariants.LG) {
    containerSize = theme.sizes[80];
  }

  return {
    height: containerSize,
    width: containerSize,
    borderRadius: containerSize,
  };
};
