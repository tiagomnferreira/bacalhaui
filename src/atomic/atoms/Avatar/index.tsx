import { SizeVariants } from "../../../style/types";
import Icon from "../Icon";
import { Container, EllipsisParagraph } from "./styled-components";
import { Props } from "./types";

export const Avatar = ({
  label,
  size = SizeVariants.MD,
  src,
  borderless = false,
  ...props
}: Props) => {
  let content;

  if (label && src === undefined)
    content = <EllipsisParagraph>{label}</EllipsisParagraph>;

  if (label === undefined && src === undefined)
    content = <Icon height="80%" width="80%" name="avatar" />;

  return (
    <Container
      size={size}
      src={src}
      role="img"
      borderless={borderless}
      {...props}
    >
      {content}
    </Container>
  );
};

export { Props as AvatarProps };
export default Avatar;
