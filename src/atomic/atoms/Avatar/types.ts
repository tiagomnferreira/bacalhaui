import { SizeVariants } from "../../../style/types";

export interface Props {
  label?: string;
  src?: string;
  readonly size?: SizeVariants.SM | SizeVariants.MD | SizeVariants.LG; // https://styled-components.com/docs/api#using-custom-props
  borderless?: boolean;
  className?: string;
}
