import styled from "styled-components";
import { WrapperProps } from "./types";
import {
  getButtonHeight,
  getButtonVariantHoverColor,
  getButtonVariantColor,
  getButtonWidth,
} from "./utils";
import { compose, height, width } from "styled-system";

const style = compose(height, width);

export const Wrapper = styled.button<WrapperProps>`
  ${getButtonVariantColor};
  height: ${getButtonHeight};
  width: ${getButtonWidth};
  border-radius: 4px;
  border: none;
  transition: all 0.2s ease-in-out;

  :hover {
    ${getButtonVariantHoverColor};
    cursor: pointer;
  }

  :active {
    transform: scale(1.03);
  }

  ${style};
`;
