import { PropsWithChildren } from "react";
import { Wrapper } from "./styled-components";
import { Props } from "./types";
import { SizeVariants, ComponentVariants } from "../../../style/types";

export const Button = ({
  children,
  variant = ComponentVariants.PRIMARY,
  size = SizeVariants.MD,
  height,
  width,
  ...props
}: PropsWithChildren<Props>) => {
  return (
    <Wrapper
      type="button"
      height={height}
      width={width}
      size={size}
      variant={variant}
      {...props}
    >
      {children}
    </Wrapper>
  );
};

export { Props as ButtonProps };
export default Button;
