import { HeightProps, WidthProps } from "styled-system";
import { SizeVariants, ComponentVariants } from "../../../style/types";
import { UnaryFn } from "../../../types";

export interface Props extends HeightProps, WidthProps {
  variant?:
    | ComponentVariants.PRIMARY
    | ComponentVariants.SECONDARY
    | ComponentVariants.ALTERNATIVE;
  className?: string;
  size?: SizeVariants.LG | SizeVariants.MD | SizeVariants.SM;
  onClick?: UnaryFn<React.MouseEvent<HTMLButtonElement>, void>;
}

export type WrapperProps = Omit<Props, "className" | "onClick">;
