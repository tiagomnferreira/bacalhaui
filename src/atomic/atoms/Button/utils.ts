import { getBoxShadow } from "../../../style/utils";
import {
  SizeVariants,
  StyledComponentsProps,
  ComponentVariants,
} from "../../../style/types";
import { WrapperProps } from "./types";

export const getButtonVariantColor = ({
  variant = ComponentVariants.PRIMARY,
  theme,
}: StyledComponentsProps<WrapperProps>) => ({
  backgroundColor: theme.variants.button[variant].bgColor,
  color: theme.variants.button[variant].color,
});

export const getButtonVariantHoverColor = ({
  variant = ComponentVariants.PRIMARY,
  theme,
}: StyledComponentsProps<WrapperProps>) => ({
  backgroundColor: theme.variants.button[variant].hover.bgColor,
  color: theme.variants.button[variant].hover.color,
  boxShadow: getBoxShadow({
    spreadRadius: -1,
    blurRadius: 5,
    color: theme.variants.button[variant].hover.bgColor,
  }),
});

export const getButtonHeight = ({
  size,
  theme,
}: StyledComponentsProps<WrapperProps>) => {
  switch (size) {
    case SizeVariants.LG:
      return theme.sizes[64];
    case SizeVariants.MD:
      return theme.sizes[40];
    case SizeVariants.SM:
      return theme.sizes[24];
    default:
      return theme.sizes[40];
  }
};

export const getButtonWidth = ({
  size,
  theme,
}: StyledComponentsProps<WrapperProps>) => {
  switch (size) {
    case SizeVariants.LG:
      return theme.sizes[160];
    case SizeVariants.MD:
      return theme.sizes[104];
    case SizeVariants.SM:
      return theme.sizes[80];
    default:
      return theme.sizes[80];
  }
};
