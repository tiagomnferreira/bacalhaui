import { ComponentStory, ComponentMeta } from "@storybook/react";
import Button from ".";
import { SizeVariants, ComponentVariants } from "../../../style/types";

export default {
  title: "Button",
  component: Button,
  argTypes: {
    variant: {
      options: [
        ComponentVariants.PRIMARY,
        ComponentVariants.SECONDARY,
        ComponentVariants.ALTERNATIVE,
      ],
      control: { type: "select" },
    },
    size: {
      options: [SizeVariants.LG, SizeVariants.MD, SizeVariants.SM],
      control: { type: "select" },
    },
    height: {
      type: "string",
      defaultValue: undefined,
    },
    width: {
      type: "string",
      defaultValue: undefined,
    },
    onClick: {
      action: "onClick callback",
      table: { disable: true },
    },
    className: {
      table: { disable: true },
    },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => (
  <Button {...args}>Click</Button>
);

export const Base = Template.bind({});
Base.args = {
  variant: ComponentVariants.PRIMARY,
  size: SizeVariants.MD,
};
