import { DefaultTheme } from "styled-components";
import { SizeVariants } from "../../../style/types";

export const getIconSize = (size: SizeVariants, theme: DefaultTheme) => {
  switch (size) {
    case SizeVariants.SM:
      return theme.sizes[16];
    case SizeVariants.MD:
      return theme.sizes[24];
    case SizeVariants.LG:
      return theme.sizes[32];
    default:
      return theme.sizes[24];
  }
};
