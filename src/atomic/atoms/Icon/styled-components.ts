import styled from "styled-components";
import { HeightProps, WidthProps, height, width } from "styled-system";

export const Wrapper = styled.div<HeightProps & WidthProps>`
  ${height};
  ${width};
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    width: 100%;
    height: 100%;
  }
`;
