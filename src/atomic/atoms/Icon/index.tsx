import { iconsList } from "./svg";
import { Props, IconName } from "./types";
import { getIconSize } from "./utils";
import { SizeVariants } from "../../../style/types";
import { Wrapper } from "./styled-components";
import { useTheme } from "styled-components";
import { get } from "lodash";
import { getColor } from "../../../style";

export const Icon = ({
  name,
  size = SizeVariants.MD,
  width,
  height,
  className,
  color = getColor("neutral", "9"),
  ...props
}: Props) => {
  const theme = useTheme();
  const iconSize = getIconSize(size, theme);

  const svgWidth = width === undefined || width === 0 ? iconSize : width;
  const svgHeight = height === undefined || height === 0 ? iconSize : height;

  const icon = get(iconsList, name, iconsList.cross);

  return (
    <Wrapper height={svgHeight} width={svgWidth} className={className}>
      {icon({ ...props, fill: color })}
    </Wrapper>
  );
};

export { Props as IconProps, IconName };
export default Icon;
