import { email } from "./email";
import { menu } from "./menu";
import { fileText } from "./fileText";
import { filePdf } from "./filePdf";
import { openFolder } from "./openFolder";
import { folder } from "./folder";
import { codfish } from "./codfish";
import { avatar } from "./avatar";
import { cross } from "./cross";
import { unknown } from "./unknown";

export const iconsList = {
  avatar,
  cross,
  codfish,
  folder,
  openFolder,
  fileText,
  menu,
  unknown,
  filePdf,
  email,
};
