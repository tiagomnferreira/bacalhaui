import { ComponentStory, ComponentMeta } from "@storybook/react";
import Icon from ".";
import { iconsList } from "./svg";
import { SizeVariants } from "../../../style/types";

export default {
  title: "Icon",
  component: Icon,
  argTypes: {
    name: {
      options: Object.keys(iconsList),
      control: { type: "select" },
    },
    size: {
      options: [SizeVariants.SM, SizeVariants.MD, SizeVariants.LG],
      control: { type: "select" },
    },
    height: {
      defaultValue: undefined,
      type: "number",
    },
    width: {
      defaultValue: undefined,
      type: "number",
    },
    color: {
      control: { type: "color" },
    },
  },
} as ComponentMeta<typeof Icon>;

const Template: ComponentStory<typeof Icon> = (args) => <Icon {...args} />;

export const Base = Template.bind({});
Base.args = {
  name: "avatar",
};
