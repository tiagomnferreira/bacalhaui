import { HeightProps, WidthProps } from "styled-system";
import { SizeVariants } from "../../../style/types";
import { iconsList } from "./svg";

export type IconName = keyof typeof iconsList;

export interface Props extends HeightProps, WidthProps {
  name: IconName;
  size?: SizeVariants.SM | SizeVariants.MD | SizeVariants.LG;
  className?: string;
  color?: string;
}
