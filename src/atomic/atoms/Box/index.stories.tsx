import { ComponentStory, ComponentMeta } from "@storybook/react";
import Box from ".";

export default {
  title: "Box",
  component: Box,
  argTypes: {},
} as ComponentMeta<typeof Box>;

const Template: ComponentStory<typeof Box> = (args) => (
  <Box {...args}>Content</Box>
);

export const Base = Template.bind({});
Base.args = {};
