import { PropsWithChildren } from "react";
import { Container } from "./styled-components";
import { Props } from "./types";

export const Box = ({ children, ...props }: PropsWithChildren<Props>) => (
  <Container {...props}>{children}</Container>
);

export { Props as BoxProps };
export default Box;
