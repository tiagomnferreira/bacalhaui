import {
  BorderRadiusProps,
  BoxShadowProps,
  HeightProps,
  MarginProps,
  PaddingProps,
  WidthProps,
} from "styled-system";

export type Props = BoxShadowProps &
  HeightProps &
  WidthProps &
  BorderRadiusProps &
  PaddingProps &
  MarginProps;
