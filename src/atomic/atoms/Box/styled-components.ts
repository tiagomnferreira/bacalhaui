import styled from "styled-components";
import {
  borderRadius,
  boxShadow,
  compose,
  height,
  margin,
  padding,
  width,
} from "styled-system";
import { Props } from "./types";
import { getBoxShadow } from "../../../style/utils";

const style = compose(boxShadow, height, width, borderRadius, padding, margin);

export const Container = styled.section<Omit<Props, "children">>`
  height: ${({ theme }) => theme.sizes[312]};
  width: ${({ theme }) => theme.sizes[408]};
  padding: ${({ theme }) => theme.spacings[8]};
  box-shadow: ${getBoxShadow({ blurRadius: 5, spreadRadius: -2 })};
  background-color: ${({ theme }) => theme.colors.neutral[0]}; 
  border-radius 4px;
  ${style};
`;
