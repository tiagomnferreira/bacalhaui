import {
  ColorProps as SSColorProps,
  TextColorProps,
  TypographyProps,
} from "styled-system";

// Styled-system patch for the color prop fixing "Types of property 'color' are incompatible"
// when applying props to component that extend ColorProps.
// https://github.com/styled-system/styled-system/issues/1206
export interface ColorProps extends Omit<SSColorProps, "color"> {
  textColor?: TextColorProps["color"];
}
export type Props = TypographyProps & ColorProps;
