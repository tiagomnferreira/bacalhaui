import { ComponentStory, ComponentMeta } from "@storybook/react";
import Paragraph from ".";

export default {
  title: "Paragraph",
  component: Paragraph,
  argTypes: {
    children: {
      defaultValue: "Default text",
      type: "string",
    },
    color: {
      type: "string",
    },
    fontSize: {
      type: "string",
    },
    fontFamily: {
      type: "string",
    },
    fontWeight: {
      options: ["bold", "bolder", "lighter", "normal"],
      control: { type: "radio" },
    },
  },
} as ComponentMeta<typeof Paragraph>;

const Template: ComponentStory<typeof Paragraph> = (args) => (
  <Paragraph {...args} />
);

export const Base = Template.bind({});
Base.args = {};
