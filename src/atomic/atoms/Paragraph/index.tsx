import { PropsWithChildren } from "react";
import { Props } from "./types";
import { Container } from "./styled-components";

export const Paragraph = ({ children, ...props }: PropsWithChildren<Props>) => (
  <Container {...props}>{children}</Container>
);

export { Props as ParagraphProps };
export default Paragraph;
