import styled from "styled-components";
import { Props } from "./types";
import { compose, typography, color as ssColor, system } from "styled-system";

// Styled-system patch for the color prop fixing "Types of property 'color' are incompatible"
// when applying props to component that extend ColorProps.
// https://github.com/styled-system/styled-system/issues/1206
export const color = compose(
  typography,
  ssColor,
  system({
    // Alias color as textColor
    textColor: {
      property: "color",
      scale: "colors",
    },
  })
);

export const Container = styled.p<Props>`
  margin: 0;
  ${typography};
  ${color};
`;
