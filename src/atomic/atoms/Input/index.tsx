import { SizeVariants } from "../../../style";
import { Addon, Container, InputWrapper } from "./styled-components";
import { Props } from "./types";

export const Input = ({
  addonAfter,
  addonBefore,
  size = SizeVariants.MD,
  borderless,
  width,
  height,
  ...props
}: Props) => (
  <Container size={size} borderless={borderless} height={height} width={width}>
    {addonBefore && <Addon>{addonBefore}</Addon>}
    <InputWrapper {...props} />
    {addonAfter && <Addon>{addonAfter}</Addon>}
  </Container>
);

export default Input;
export { Props as InputProps };
