import { ComponentStory, ComponentMeta } from "@storybook/react";
import Input from ".";
import Icon from "../Icon";

export default {
  title: "Input",
  component: Input,
  argTypes: {
    addonAfter: {
      table: { disable: true },
    },
    addonBefore: {
      table: { disable: true },
    },
    height: {
      type: "string",
    },
    width: {
      type: "string",
    },
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

export const Base = Template.bind({});
Base.args = {};

export const WithAddons = Template.bind({});
WithAddons.args = {
  addonAfter: <Icon name="avatar" />,
  addonBefore: <Icon name="codfish" />,
};
