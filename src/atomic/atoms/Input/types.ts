import { HTMLAttributes } from "react";
import { SizeVariants } from "../../../style";
import { HeightProps, WidthProps } from "styled-system";

export interface Props
  extends Omit<HTMLAttributes<HTMLInputElement>, "size">,
    WidthProps,
    HeightProps {
  addonBefore?: JSX.Element;
  addonAfter?: JSX.Element;
  size?: SizeVariants.SM | SizeVariants.MD | SizeVariants.LG;
  borderless?: boolean;
}
