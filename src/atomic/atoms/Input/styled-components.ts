import { Props } from "./types";
import styled from "styled-components";
import { height, width } from "styled-system";
import { getFontSize, getHeight, getWidth } from "./utils";
import { getBoxShadow } from "../../../style";

export const Container = styled.div<
  Pick<Props, "size" | "borderless" | "width" | "height">
>`
  height: ${getHeight};
  width: ${getWidth};
  border-radius: ${({ theme }) => theme.spacings[4]};
  box-shadow: ${({ borderless }) =>
    !borderless && getBoxShadow({ blurRadius: 5, spreadRadius: -2 })};
  box-sizing: border-box;
  padding: ${({ theme }) => theme.spacings[4]};
  display: flex;
  align-items: center;
  ${width};
  ${height};

  input {
    font-size: ${getFontSize};
  }
`;

export const InputWrapper = styled.input`
  height: 90%;
  width: 100%;
  box-sizing: border-box;
  appearance: none;
  border: none;
`;

export const Addon = styled.div`
  width: 20%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`;
