import { Props } from "./types";
import { SizeVariants, StyledComponentsProps } from "../../../style";

export const getHeight = ({ size, theme }: StyledComponentsProps<Props>) => {
  switch (size) {
    case SizeVariants.LG:
      return theme.sizes[64];
    case SizeVariants.MD:
      return theme.sizes[32];
    case SizeVariants.SM:
      return theme.sizes[24];
    default:
      return theme.sizes[32];
  }
};

export const getWidth = ({ size, theme }: StyledComponentsProps<Props>) => {
  switch (size) {
    case SizeVariants.LG:
      return theme.sizes[424];
    case SizeVariants.MD:
      return theme.sizes[328];
    case SizeVariants.SM:
      return theme.sizes[248];
    default:
      return theme.sizes[328];
  }
};

export const getFontSize = ({ size, theme }: StyledComponentsProps<Props>) => {
  switch (size) {
    case SizeVariants.LG:
      return theme.sizes[32];
    case SizeVariants.MD:
      return theme.sizes[16];
    case SizeVariants.SM:
      return theme.sizes[8];
    default:
      return theme.sizes[16];
  }
};
