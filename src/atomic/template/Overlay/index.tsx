import { PropsWithChildren } from "react";
import { Container } from "./styled-components";
import { Props } from "./types";

export const Overlay = ({
  children,
  onClick,
  className,
}: PropsWithChildren<Props>) => {
  const handleClick = (e) => {
    e.stopPropagation();
    if (onClick) onClick(e);
  };

  return (
    <Container className={className} onClick={handleClick}>
      {children}
    </Container>
  );
};

export { Props as OverlayProps };
export default Overlay;
