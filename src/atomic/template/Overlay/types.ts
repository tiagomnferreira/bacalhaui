import { UnaryFn } from "../../../types";

export interface Props {
  onClick?: UnaryFn<React.MouseEvent<HTMLDivElement>, void>;
  className?: string;
}
