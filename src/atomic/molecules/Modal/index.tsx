import { PropsWithChildren } from "react";
import {
  CloseIcon,
  Container,
  Content,
  StyledOverlay,
} from "./styled-components";
import { Props } from "./types";
import Header from "./Header";
import Footer from "./Footer";
import { useTheme } from "styled-components";

export const Modal = ({
  isOpen,
  children,
  className,
  customHeader,
  onClose,
  onOverlayClick,
  hasCloseIcon = false,
  title,
  isOverlayVisible,
  customFooter,
  animations = false,
  ...props
}: PropsWithChildren<Props>) => {
  const { sizes } = useTheme();

  if (!isOpen) return null;

  const handleStopPropagation = (e) => {
    e.stopPropagation();
  };

  return (
    <StyledOverlay
      onClick={onOverlayClick}
      isOverlayVisible={isOverlayVisible}
      animations={animations}
    >
      <Container
        className={className}
        role="combobox"
        aria-label="modal"
        onClick={handleStopPropagation}
        {...props}
      >
        {hasCloseIcon && (
          <CloseIcon name="cross" height={sizes[8]} width={sizes[8]} />
        )}
        <Header
          title={title}
          customHeader={customHeader}
          hasCloseIcon={hasCloseIcon}
        />
        <Content>{children}</Content>
        <Footer customFooter={customFooter} />
      </Container>
    </StyledOverlay>
  );
};

export default Modal;
export { Props as ModalProps };
