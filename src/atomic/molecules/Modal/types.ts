import {
  BackgroundProps,
  BorderRadiusProps,
  MaxHeightProps,
  MaxWidthProps,
  MinHeightProps,
  MinWidthProps,
} from "styled-system";
import { NullaryFn } from "../../../types";

export type ContainerProps = BackgroundProps &
  BorderRadiusProps &
  MaxWidthProps &
  MaxHeightProps &
  MinHeightProps &
  MinWidthProps;

export interface Props extends ContainerProps {
  isOpen: boolean;
  onClose: NullaryFn<void>;
  hasCloseIcon?: boolean;
  className?: string;
  customHeader?: JSX.Element | null;
  customFooter?: JSX.Element | null;
  onOverlayClick?: NullaryFn<void>;
  title?: string;
  isOverlayVisible?: boolean;
  animations?: boolean;
}

export interface HeaderProps
  extends Pick<Props, "title" | "customHeader" | "hasCloseIcon"> {}

export interface FooterProps extends Pick<Props, "customFooter"> {}
