import { FooterContainer } from "../styled-components";
import { FooterProps } from "../types";

const Footer = ({ customFooter }: FooterProps) => {
  if (customFooter === null) return null;
  if (customFooter) return customFooter;

  return <FooterContainer />;
};

export default Footer;
