import { HeaderContainer } from "../styled-components";
import { HeaderProps } from "../types";

const Header = ({ customHeader, title, hasCloseIcon }: HeaderProps) => {
  if (customHeader === null) return null;

  if (customHeader) return customHeader;

  return <HeaderContainer hasCloseIcon={hasCloseIcon}>{title}</HeaderContainer>;
};

export default Header;
