import { ComponentStory, ComponentMeta } from "@storybook/react";
import Modal from ".";

export default {
  title: "Modal",
  component: Modal,
  argTypes: {
    isOpen: {
      defaultValue: true,
      type: "boolean",
    },
    hasCloseIcon: {
      defaultValue: true,
      type: "boolean",
    },
    onClose: {
      table: { disable: true },
    },
    onOverlayClick: {
      table: { disable: true },
    },
    customFooter: {
      table: { disable: true },
    },
    customHeader: {
      table: { disable: true },
    },
    className: {
      table: { disable: true },
    },
  },
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

export const Base = Template.bind({});
Base.args = {
  children: "Content",
  onClose: () => null,
};

export const WithCustomHeader = Template.bind({});
WithCustomHeader.args = {
  children: "Content",
  customHeader: <div>this is a custom header with no styling at all</div>,
};

export const WithCustomFooter = Template.bind({});
WithCustomFooter.args = {
  children: "Content",
  customFooter: <div>this is a custom footer with no styling at all</div>,
};

export const WithLongText = Template.bind({});
WithLongText.args = {
  children: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor neque eget sem sollicitudin fringilla a vel ligula. Vivamus suscipit consectetur est in tincidunt. Morbi fermentum fringilla velit, a hendrerit erat lobortis sed. Donec in odio ligula. In gravida justo ut est hendrerit hendrerit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris quis lorem sed augue viverra convallis. Fusce dictum ligula et eros tempus, ut eleifend tellus egestas. Suspendisse potenti. Duis posuere consectetur sagittis. Phasellus vitae gravida erat.

    Proin dolor urna, sodales at nisi luctus, porttitor pretium tellus. Vestibulum dui velit, lobortis in elit in, dignissim elementum sapien. Ut vitae nulla id mauris molestie sodales a et odio. Duis augue enim, placerat sed aliquam nec, convallis luctus mi. Quisque in sagittis dui. Fusce porttitor ultrices massa nec molestie. Suspendisse consequat in augue non vehicula.
    
    In nec ullamcorper lacus, sit amet suscipit purus. Praesent semper libero dui, ac placerat nibh vulputate vitae. Cras suscipit nisi eu enim mollis finibus. Quisque finibus, felis vel ullamcorper fermentum, eros ante ullamcorper elit, vitae imperdiet mi mi et velit. Proin pulvinar lacus at iaculis ultricies. Donec ut ex et lacus aliquam malesuada ut sed massa. Aenean enim felis, dictum vitae malesuada sit amet, semper nec dolor. Nullam consequat in dolor eget vestibulum. Cras rutrum leo sed erat luctus molestie vitae at dui. Mauris felis turpis, pharetra nec bibendum vel, maximus vitae felis. Mauris vestibulum nunc nibh, non vestibulum tortor viverra quis. Nam bibendum, tellus sit amet bibendum dapibus, felis tellus egestas sapien, at gravida ipsum nibh sit amet metus. Vestibulum semper pretium velit. Donec ac elit dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.`,
};
