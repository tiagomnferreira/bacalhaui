import styled, { css, keyframes } from "styled-components";
import { Overlay } from "../../template";
import { Icon } from "../../atoms";
import {
  background,
  borderRadius,
  maxHeight,
  maxWidth,
  minHeight,
  minWidth,
} from "styled-system";
import { ContainerProps, Props } from "./types";
import { adjustColorOpacity, getBoxShadow } from "../../../style";

const opening = keyframes`
    from {
        opacity: 0;
    }
    to {
        opacity: 1;
    }
`;

export const Container = styled.div<ContainerProps>`
  position: relative;
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 90%;
  min-height: ${({ theme }) => theme.sizes[200]};
  max-width: ${({ theme }) => theme.sizes[472]};
  max-height: ${({ theme }) => theme.sizes[328]};
  background-color: ${({ theme }) => theme.colors.neutral[0]};
  border-radius: ${({ theme }) => theme.spacings[4]};
  box-shadow: ${getBoxShadow({ blurRadius: 5, spreadRadius: -2 })};
  ${maxWidth};
  ${maxHeight};
  ${minHeight};
  ${minWidth};
  ${borderRadius};
  ${background};
`;

export const HeaderContainer = styled.div<Pick<Props, "hasCloseIcon">>`
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: ${({ theme }) => theme.sizes[48]};
  border-bottom: 1px solid ${({ theme }) => theme.colors.neutral[2]};
  padding: ${({ theme, hasCloseIcon }) =>
    !hasCloseIcon
      ? theme.spacings[4]
      : `${theme.spacings[4]} ${theme.spacings[48]} ${theme.spacings[4]} ${theme.spacings[4]}`};
`;

export const Content = styled.div`
  flex: 1;
  padding: ${({ theme }) => theme.spacings[4]};
  overflow: auto;
`;

export const FooterContainer = styled.div`
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: ${({ theme }) => theme.sizes[32]};
  padding: ${({ theme }) => theme.spacings[4]};
  border-top: 1px solid ${({ theme }) => theme.colors.neutral[2]};
`;

export const StyledOverlay = styled(Overlay)<
  Pick<Props, "isOverlayVisible" | "animations">
>`
  top: 0;
  background-color: ${({ isOverlayVisible }) =>
    isOverlayVisible && adjustColorOpacity("neutral", "4", 50)};
  display: flex;
  animation: ${({ animations }) =>
    animations &&
    css`
      ${opening} 0.5s
    `};
`;

export const CloseIcon = styled(Icon)`
  position: absolute;
  right: ${({ theme }) => theme.spacings[12]};
  top: ${({ theme }) => theme.spacings[12]};
  cursor: pointer;
  padding: ${({ theme }) => theme.spacings[8]};
  border-radius: ${({ theme }) => theme.spacings[20]};
  transition: background-color 0.2s ease-in-out;

  :hover {
    background-color: ${adjustColorOpacity("neutral", "1", 50)};
  }

  :active {
    background-color: ${({ theme }) => theme.colors.neutral[1]};
  }
`;
