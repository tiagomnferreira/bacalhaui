export type BoxPosition =
  | "bottom-right"
  | "bottom-left"
  | "top-right"
  | "top-left"
  | "left-top"
  | "left-bottom"
  | "right-top"
  | "right-bottom";

export type ClickTrigger = "click" | "context-menu";

export interface Props {
  content: JSX.Element | JSX.Element[];
  clickAndClose?: boolean;
  position?: BoxPosition;
  trigger?: ClickTrigger;
  noStyleDropdown?: boolean;
}
