import { UnaryFn } from "../../../../types";
import { BoxPosition } from "../types";

export interface Props {
  isOpen: boolean;
  onClick: UnaryFn<React.MouseEvent<HTMLDivElement>, void>;
  controllerHeight: number | undefined;
  controllerWidth: number | undefined;
  position: BoxPosition;
  noStyleDropdown: boolean;
}

export type ContentStyledProps = Pick<
  Props,
  "controllerHeight" | "position" | "controllerWidth" | "noStyleDropdown"
>;
