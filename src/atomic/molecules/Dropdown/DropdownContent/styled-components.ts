import styled, { css } from "styled-components";
import { ContentStyledProps } from "./types";
import { getContentPosition } from "../utils";
import { getBoxShadow } from "../../../../style";

const style = css<ContentStyledProps>`
  border: ${({ theme }) => `1px solid ${theme.colors.neutral[2]}`};
  background-color: ${({ theme }) => theme.colors.neutral[1]};
  border-radius: ${({ theme }) => theme.spacings[4]};
  box-shadow: ${getBoxShadow({ blurRadius: 4, spreadRadius: -1 })};
`;

export const Container = styled.div<ContentStyledProps>`
  position: absolute;
  min-width: ${({ theme }) => theme.sizes[408]};
  ${({ theme, controllerWidth, controllerHeight, position }) =>
    getContentPosition(theme, controllerHeight, controllerWidth, position)};
  ${({ noStyleDropdown }) => !noStyleDropdown && style};
`;
