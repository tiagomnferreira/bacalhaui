import { ForwardedRef, PropsWithChildren, forwardRef } from "react";
import { Props } from "./types";
import { Container } from "./styled-components";

const DropdownContent = forwardRef(
  (
    {
      isOpen,
      children,
      onClick,
      controllerHeight,
      controllerWidth,
      position,
      noStyleDropdown,
    }: PropsWithChildren<Props>,
    ref: ForwardedRef<HTMLDivElement | null>
  ) =>
    isOpen ? (
      <Container
        onClick={onClick}
        controllerHeight={controllerHeight}
        controllerWidth={controllerWidth}
        position={position}
        ref={ref}
        noStyleDropdown={noStyleDropdown}
      >
        {children}
      </Container>
    ) : null
);

export default DropdownContent;
