import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  position: relative;
  width: min-content;
  height: min-content;
  z-index: 999;
`;

export const Controller = styled.div`
  cursor: pointer;
`;
