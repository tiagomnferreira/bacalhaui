import { PropsWithChildren, useRef, useState } from "react";
import { Props } from "./types";
import { Container, Controller } from "./styled-components";
import DropdownContent from "./DropdownContent";
import { Overlay } from "../../template";

export const Dropdown = ({
  children,
  content,
  clickAndClose = true,
  position = "bottom-left",
  trigger = "click",
  noStyleDropdown = false,
}: PropsWithChildren<Props>) => {
  const [isOpen, setOpen] = useState(false);
  const controllerRef = useRef<HTMLDivElement | null>(null);
  const contentRef = useRef<HTMLDivElement | null>(null);

  const handleContentClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    if (clickAndClose) {
      setOpen(false);
    }
  };

  const onClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    if (trigger == "click") setOpen(!isOpen);
  };

  const onContextMenu = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    if (trigger == "context-menu") setOpen(true);
  };

  const renderContent = (
    <Container role="combobox">
      <Controller
        onClick={onClick}
        onContextMenu={onContextMenu}
        ref={controllerRef}
      >
        {children}
      </Controller>
      <DropdownContent
        ref={contentRef}
        isOpen={isOpen}
        onClick={handleContentClick}
        controllerHeight={controllerRef.current?.clientHeight}
        controllerWidth={controllerRef.current?.clientWidth}
        position={position}
        noStyleDropdown={noStyleDropdown}
      >
        {content}
      </DropdownContent>
    </Container>
  );

  if (!isOpen) return renderContent;

  return (
    <>
      {renderContent}
      <Overlay onClick={() => setOpen(false)} />
    </>
  );
};

export { Props as DropdownProps };
export default Dropdown;
