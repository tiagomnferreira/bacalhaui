import { Theme } from "../../../style";
import { BoxPosition } from "./types";

// export const validatePosition = (position:BoxPosition) => {}

export const getContentPosition = (
  theme: Theme,
  controllerHeight: number | undefined,
  controllerWidth: number | undefined,
  position: BoxPosition
) => {
  if (position === "top-left")
    return {
      bottom: controllerHeight + "px",
      left: 0,
      marginBottom: theme.spacings[4],
    };

  if (position === "top-right")
    return {
      bottom: controllerHeight + "px",
      right: 0,
      marginBottom: theme.spacings[4],
    };

  if (position === "left-bottom")
    return {
      right: controllerWidth + "px",
      bottom: 0,
      marginRight: theme.spacings[4],
    };

  if (position === "left-top")
    return {
      right: controllerWidth + "px",
      top: 0,
      marginRight: theme.spacings[4],
    };

  if (position === "right-bottom")
    return {
      left: controllerWidth + "px",
      bottom: 0,
      marginLeft: theme.spacings[4],
    };

  if (position === "right-top")
    return {
      left: controllerWidth + "px",
      top: 0,
      marginLeft: theme.spacings[4],
    };

  if (position === "bottom-right")
    return {
      top: controllerHeight + "px",
      right: 0,
      marginTop: theme.spacings[4],
    };

  return {
    top: controllerHeight + "px",
    left: 0,
    marginTop: theme.spacings[4],
  };
};
