import { ComponentStory, ComponentMeta } from "@storybook/react";
import Dropdown from ".";
import { Button, Icon } from "../../atoms";

export default {
  title: "Dropdown",
  component: Dropdown,
  argTypes: {
    children: {
      table: { disable: true },
    },
    content: {
      table: { disable: true },
      defaultValue: (
        <article>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
          <button onClick={() => window.alert("CLICK")}>click</button>
        </article>
      ),
    },
  },
} as ComponentMeta<typeof Dropdown>;

const Template: ComponentStory<typeof Dropdown> = (args) => (
  <Dropdown {...args} />
);

export const WithButton = Template.bind({});
WithButton.args = {
  children: <Button>click</Button>,
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  children: <Icon name="avatar" />,
};
