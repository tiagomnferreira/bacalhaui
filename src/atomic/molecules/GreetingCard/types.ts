import { AvatarProps } from "../../atoms";

export interface Props {
  avatarConfig: AvatarProps;
  title: string;
  description?: string;
}
