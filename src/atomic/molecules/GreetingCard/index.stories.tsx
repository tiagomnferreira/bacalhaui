import { ComponentStory, ComponentMeta } from "@storybook/react";
import UserCard from ".";

export default {
  title: "UserCard",
  component: UserCard,
  argTypes: {},
} as ComponentMeta<typeof UserCard>;

const Template: ComponentStory<typeof UserCard> = (args) => (
  <UserCard {...args} />
);

export const Base = Template.bind({});
Base.args = {
  title: "Tiago Ferreira",
};
