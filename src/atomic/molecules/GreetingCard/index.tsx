import { useTheme } from "styled-components";
import { Avatar, Paragraph } from "../../atoms";
import { Container, Info } from "./styled-components";
import { Props } from "./types";

export const UserCard = ({ avatarConfig, title, description }: Props) => {
  const theme = useTheme();

  return (
    <Container>
      <Avatar borderless {...avatarConfig} />
      <Info>
        <Paragraph
          fontSize={theme.sizes[!!description ? 16 : 24]}
          fontWeight={!!description ? "bold" : "normal"}
        >
          {title}
        </Paragraph>
        <Paragraph fontSize={theme.sizes[16]}>{description}</Paragraph>
      </Info>
    </Container>
  );
};

export default UserCard;
