import styled from "styled-components";
import { getBoxShadow } from "../../../style";

export const Container = styled.section`
  display: flex;
  gap: ${({ theme }) => theme.spacings[8]};
  width: ${({ theme }) => theme.sizes[312]};
  box-shadow: ${getBoxShadow({ blurRadius: 5, spreadRadius: -2 })};
  border-radius: ${({ theme }) => theme.sizes[40]};
  padding: ${({ theme }) => `0 ${theme.spacings[28]}`};
  box-sizing: border-box;
`;

export const Info = styled.article`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1;
`;
