import colors from "./palette";
import sizes from "./sizes";
import spacings from "./spacings";
import { Theme } from "../types";
import variants from "./variants";

const theme: Theme = {
  colors,
  spacings,
  sizes,
  variants,
};

export default theme;
