import { getColor, adjustColorBrightness } from "../utils";
import { ButtonVariants } from "../types";

const button: ButtonVariants = {
  primary: {
    color: getColor("neutral", "0"),
    bgColor: getColor("blue", "9"),
    hover: {
      color: getColor("neutral", "0"),
      bgColor: adjustColorBrightness("blue", "9", -7),
    },
  },
  secondary: {
    color: getColor("neutral", "9"),
    bgColor: getColor("yellow", "9"),
    hover: {
      color: getColor("neutral", "9"),
      bgColor: adjustColorBrightness("yellow", "9", -7),
    },
  },
  alternative: {
    color: getColor("neutral", "0"),
    bgColor: getColor("red", "9"),
    hover: {
      color: getColor("neutral", "0"),
      bgColor: adjustColorBrightness("red", "9", -7),
    },
  },
};

const variants = {
  button,
};

export default variants;
