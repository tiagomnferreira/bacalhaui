import { DefaultTheme, ThemedStyledProps } from "styled-components";

export interface Theme {
  colors: Pallete;
  spacings: Spacings;
  sizes: Sizes;
  variants: Variants;
}

export interface Variants {
  button: ButtonVariants;
}

export enum SizeVariants {
  XXS = "extra-extra-small",
  XS = "extra-small",
  SM = "small",
  MD = "medium",
  LG = "large",
  XL = "extra-large",
  XXL = "extra-extra-large",
}

export enum ComponentVariants {
  PRIMARY = "primary",
  SECONDARY = "secondary",
  ALTERNATIVE = "alternative",
  DANGER = "danger",
  WARNING = "warning",
  INFO = "info",
  SUCCESS = "success",
}

export interface BoxShadow {
  offsetX?: number;
  offsetY?: number;
  blurRadius?: number;
  spreadRadius?: number;
  color?: string;
}

export interface ButtonVariantColors {
  color: string;
  bgColor: string;
}

export interface ButtonVariantAttributes extends ButtonVariantColors {
  hover: ButtonVariantColors;
}

export type StyledComponentsProps<T> = ThemedStyledProps<T, DefaultTheme>;
export type ButtonVariants = Record<
  | ComponentVariants.PRIMARY
  | ComponentVariants.SECONDARY
  | ComponentVariants.ALTERNATIVE,
  ButtonVariantAttributes
>;
export type Sizes = Record<SizeKeys, string>;
export type Spacings = Record<SpacingsKeys, string>;
export type ColorVariation = Record<ColorVariationKey, string>;
export type Pallete = Record<Colors, ColorVariation>;
export type VariantComponents = keyof Variants;
export type SizeKeys =
  | "8"
  | "16"
  | "24"
  | "32"
  | "40"
  | "48"
  | "56"
  | "64"
  | "72"
  | "80"
  | "88"
  | "96"
  | "104"
  | "112"
  | "120"
  | "128"
  | "136"
  | "144"
  | "152"
  | "160"
  | "168"
  | "176"
  | "184"
  | "192"
  | "200"
  | "216"
  | "232"
  | "248"
  | "264"
  | "280"
  | "296"
  | "312"
  | "328"
  | "344"
  | "360"
  | "376"
  | "392"
  | "408"
  | "424"
  | "440"
  | "456"
  | "472"
  | "488";

export type SpacingsKeys =
  | "none"
  | "4"
  | "8"
  | "12"
  | "16"
  | "20"
  | "24"
  | "28"
  | "32"
  | "36"
  | "40"
  | "44"
  | "48"
  | "52";

export type ColorVariationKey =
  | "0"
  | "1"
  | "2"
  | "3"
  | "4"
  | "5"
  | "6"
  | "7"
  | "8"
  | "9";

export type Colors =
  | "blue"
  | "red"
  | "neutral"
  | "yellow"
  | "green"
  | "purple"
  | "pink";
