import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle({
  fontFamily: "inherit",
});
