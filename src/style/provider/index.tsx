import { PropsWithChildren } from "react";
import { ThemeProvider } from "styled-components";
import theme from "../theme";
import { GlobalStyle } from "../globalStyle";

export const BacalhaUIThemeProvider = ({ children }: PropsWithChildren) => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    {children}
  </ThemeProvider>
);

export default BacalhaUIThemeProvider;
