import { get } from "lodash";
import palette from "./theme/palette";
import { BoxShadow, ColorVariationKey, Colors } from "./types";

export const getColor = (color: Colors, variation: ColorVariationKey) =>
  get(palette[color], variation, palette.neutral[0]);

export const getBoxShadow = ({
  offsetX = 0,
  offsetY = 0,
  blurRadius = 0,
  spreadRadius = 0,
  color = getColor("neutral", "9"),
}: BoxShadow) =>
  `${offsetX}px ${offsetY}px ${blurRadius}px ${spreadRadius}px ${color}`;

export const padHexCode = (hex: string) => (hex.length === 1 ? `0${hex}` : hex);

export const adjustColorBrightness = (
  color: Colors,
  variation: ColorVariationKey,
  amount: number
): string => {
  const paletteColor = getColor(color, variation);

  // Parse the hexadecimal color value into separate red, green, and blue components
  const r = parseInt(paletteColor.substring(1, 3), 16);
  const g = parseInt(paletteColor.substring(3, 5), 16);
  const b = parseInt(paletteColor.substring(5, 7), 16);

  // Calculate the new RGB values
  const newR = Math.max(0, Math.min(255, r + amount));
  const newG = Math.max(0, Math.min(255, g + amount));
  const newB = Math.max(0, Math.min(255, b + amount));

  // Convert the new RGB values back to a hexadecimal color string
  const newColor = `#${padHexCode(newR.toString(16))}${padHexCode(
    newG.toString(16)
  )}${padHexCode(newB.toString(16))}`;

  // Return the new hexadecimal color string
  return newColor;
};

export const adjustColorOpacity = (
  color: Colors,
  variation: ColorVariationKey,
  opacity: number
) => `${getColor(color, variation)}${opacity}`;
