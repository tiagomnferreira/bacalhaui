# BacalhaUI

Design-system library build with React

## How to setup

Wrap your app with our provider `BacalhaUIThemeProvider`. You also have access to the Theme interface by importing `IBacalhaUITheme`.

### Packages

- React
- styled-components
- styled-system
- lodash
- storybook

## Try it out

https://bacalhaui.netlify.app/
